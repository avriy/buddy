import Foundation
import CoreLocation

/// Represents event that user is able to attend.
struct Event {
  /// General type of event.
  enum EventType {
    /// Something requiring food to eat, drinks.
    case meal
    /// Some sport activity.
    case activity
  }
  /// Main title of event.
  let title: String
  /// Detailed explanation of what `Event` would be.
  let description: String
  /// Type of the event.
  let eventType: EventType
  /// Location.
  let locationCoordinate: CLLocationCoordinate2D
}

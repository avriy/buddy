#if DEBUG
import CoreLocation

extension Event {
  static let starbucks = Event(
    title: "Coffee at Starbucks",
    description: "Casual chat by the cup of coffee, a? We need a long text here",
    eventType: .meal,
    locationCoordinate: CLLocationCoordinate2D(latitude: 51.492788, longitude: -0.224478))
  
  static let badminton = Event(
    title: "Badminton",
    description: "Playing badminton near Google",
    eventType: .activity,
    locationCoordinate: CLLocationCoordinate2D(latitude: 51.537597, longitude: -0.128025))
}
#endif

import SwiftUI
import MapKit

struct MapView: UIViewRepresentable {
  var event: Event
  
  func makeUIView(context: Context) -> MKMapView {
    MKMapView(frame: .zero)
  }
  
  func updateUIView(_ view: MKMapView, context: Context) {
    let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
    let region = MKCoordinateRegion(center: event.locationCoordinate, span: span)
    view.setRegion(region, animated: true)
    view.addAnnotation(EventAnnotation(event: event))
  }
  
  private class EventAnnotation: NSObject, MKAnnotation {
    @objc dynamic var coordinate: CLLocationCoordinate2D
    var title: String?
    
    init(event: Event) {
      self.coordinate = event.locationCoordinate
      self.title = event.title
    }
  }
}

#if DEBUG
struct MapView_Previews: PreviewProvider {
  static var previews: some View {
    MapView(event: .starbucks)
  }
}
#endif

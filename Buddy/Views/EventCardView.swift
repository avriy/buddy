import SwiftUI

struct EventCardView : View {
  let action: () -> Void = { }
  @State var event: Event
  
  var body: some View {
      HStack {
        MapView(event: event)
          .aspectRatio(1, contentMode: .fit)
          .frame(width: 120)
          .clipShape(Circle())
        
        VStack(alignment: .leading) {
          Text(event.title)
            .font(.title)
          Text(event.description)
            .font(.subheadline)
            .lineLimit(4)
          
          Text(event.typeString)
          
          HStack {
            Spacer()
            Button(action: {
              print("Did press on button")
            }) {
              Text("Go")
            }
          }.padding([.trailing])
        }
      }
      .background(Color.white, cornerRadius: 10)
    
    
//    RoundedRectangle(cornerRadius: 10)
//      .foregroundColor(.white)
//      .shadow(radius: 10)
//    .overlay(
//    )
  }
}

extension Event {
  var typeString: String {
    switch eventType {
    case .activity: return "🏄‍♀️"
    case .meal: return "☕️"
    }
  }
}

#if DEBUG
struct EventCardView_Previews : PreviewProvider {
  static var previews: some View {
    EventCardView(event: .starbucks)
      .previewLayout(.fixed(width: 380, height: 180))
  }
}
#endif

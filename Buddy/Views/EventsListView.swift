import SwiftUI

struct EventsListView : View {
  @State var events: [Event]
  
  var body: some View {
    NavigationView {
      List(events.identified(by: \.title)) { event in
        EventCardView(event: event)
          .frame(height: 160)
      }.listStyle(.grouped)
      .navigationBarTitle(Text("Events"))
    }
  }
}

#if DEBUG
struct EventsListView_Previews : PreviewProvider {
  static var previews: some View {
    EventsListView(events: [.starbucks, .badminton])
  }
}
#endif

